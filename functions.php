<?php

const BOOK_FILE = "books.txt";
const AUTHOR_FILE = "authors.txt";
function getBookData() {
    $lines = file(BOOK_FILE);
    $bookData = [];

    foreach ($lines as $line) {
        list($title, $author1, $author2, $grade, $isRead) = explode(";", $line);
        $bookData[] = ["title" => urldecode($title), "author1" => urldecode($author1), "author2" => urldecode($author2), "grade" => urldecode($grade), "isRead" => urldecode($isRead)];
    }

    return $bookData;
}

function addBook($title, $author1, $author2, $grade, $isRead) {
    if ($title) {
        $line = urlencode($title) . ";" . urlencode($author1) . ";" . urlencode($author2) . ";" . urlencode($grade) . ";" . urlencode($isRead) .PHP_EOL;
        file_put_contents(BOOK_FILE, $line, FILE_APPEND);
    }
}

function getAuthorData() {
    $lines = file(AUTHOR_FILE);
    $authorData = [];

    foreach ($lines as $line) {
        list($firstName, $lastName, $grade) = explode(";", $line);
        $authorData[] = ["firstName" => urldecode($firstName), "lastName" => urldecode($lastName), "grade" => urldecode($grade)];
    }

    return $authorData;
}

function addAuthor($firstName, $lastName, $grade) {
    $line = urlencode($firstName) . ";" . urlencode($lastName) . ";" . urlencode($grade) . PHP_EOL;
    file_put_contents(AUTHOR_FILE, $line, FILE_APPEND);
}

function editBook($title, $old_title, $grade, $delete, $isRead) {
    $bookData = getBookData();
    file_put_contents("books.txt", "");
    foreach ($bookData as $book) {
        if ($book["title"] == $old_title) {
            if (! $delete) {
                addBook($title, $book["author1"], $book["author2"], $grade, $isRead);
            }
        } else {
            addBook($book["title"], $book["author1"], $book["author2"], $book["grade"], $isRead);
        }
    }

}
function editAuthor($firstName, $lastName, $old_fname, $old_lname, $grade, $delete){
    $authorData = getAuthorData();
    file_put_contents("authors.txt", "");
    foreach ($authorData as $author) {
        if ($author["firstName"] == $old_fname || $author['lastName'] == $old_lname) {
            if (! $delete) {
                addAuthor($firstName, $lastName, $grade);
            }
        } else {
            addAuthor($author["firstName"], $author["lastName"], $author["grade"]);
        }
    }
}