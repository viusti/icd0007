<?php
require_once "./DAO/AuthorDAO.php";
require_once "./DAO/Database.php";
require_once "./DTO/Author.php";
$authors = new AuthorDAO();
$authorData = $authors->getAuthors();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Muuda Raamat</title>

    <link href="styles.css" rel="stylesheet">

</head>
<body>
<nav>
    <a id="book-list-link" href="index.php">Raamatud</a>
    <span>|</span>
    <a id="book-form-link" href="book-add.php">Lisa raamat</a>
    <span>|</span>
    <a id="author-list-link" href="author-list.php">Autorid</a>
    <span>|</span>
    <a id="author-form-link" href="author-add.php">Lisa autor</a>
</nav>
<?php if (isset($_GET["error"])) : ?>
    <div id="error-block"> Pealkiri peab olema 3 kuni 23 märki! </div>
<?php endif; ?>

<main>
    <form action="books.php" method="post">
        <label for="title">Pealkiri:</label>

        <input type="text" id="title" name="title" value="<?= urldecode($_GET["old_title"]) ?>"><br><br>

        <label for="author1">Autor 1:</label>
        <select name="author1" id="author1">
            <option></option>
            <?php foreach ($authorData as $author): ?>
                <?php if ($author->id == $_GET["author1"]):?>
                    <option selected value="<?= $author->id?>"><?= $author->fname . " " . $author->lname ?></option>>
                <?php else: ?>
                    <option value="<?= $author->id?>"><?= $author->fname . " " . $author->lname ?></option>>
                        <?php endif; ?>

            <?php endforeach; ?>
        </select>
        <br><br>

        <label for="author2">Autor 2:</label>
        <select name="author2" id="author2">
            <option></option>
            <?php foreach ($authorData as $author): ?>
                <?php if ($author->id == $_GET["author2"]):?>
                    <option selected value="<?= $author->id?>"><?= $author->fname . " " . $author->lname ?></option>>
                <?php else: ?>
                    <option value="<?= $author->id?>"><?= $author->fname . " " . $author->lname ?></option>>
                <?php endif; ?>

            <?php endforeach; ?>
        </select>

        <input type="hidden" id="old_title" name="old_title" value="<?= $_GET["old_title"]?>">
        <input type="hidden" id="oldAuthor1" name="oldAuthor1" value="<?= $_GET["author1"]?>">
        <input type="hidden" id="oldAuthor2" name="oldAuthor2" value="<?= $_GET["author2"]?>">
        <input type="hidden" id="id" name="id" value="<?= (isset($_GET["id"])) ? $_GET["id"] : null?>">

        <label>Hinne:</label>

        <?php for ($i = 1; $i <= 5; $i++): ?>
            <input <?php echo (isset($_GET["grade"]) && ($_GET["grade"] == $i)) ? "checked" : ""?> type="radio" name="grade" value="<?= $i ?>"><?= $i ?>
        <?php endfor; ?>
        <br><br>

        <label for="isRead">Loetud:</label>

        <input id="isRead" name="isRead" type="checkbox" <?php echo ($_GET["isRead"]) ? "checked" : ""?>>

        <br><br>

        <input name="deleteButton" type="submit" value="Kustuta">
        <input name="submitButton" type="submit" value="Salvesta">
    </form>
</main>
<footer>
    <p>ICD0007 Harjutustunni leht</p>
</footer>
</body>
</html>