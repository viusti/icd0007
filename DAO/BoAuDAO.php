<?php
class BoAuDAO
{
    public $database;
    public function __construct()
    {
        $this->database = new Database();
    }

    public function add($bookauthor)
    {
        $stmt = $this->database->connection->prepare('INSERT INTO Book_Author (Books_ID, Authors_ID) VALUES (:BooksID, :AuthorsID);');
        $stmt->bindValue(':BooksID', $bookauthor-> bookId);
        $stmt->bindValue(':AuthorsID', $bookauthor-> authorId);
        $stmt->execute();
        $id = $this->database->connection->lastInsertId();
        $bookauthor->id = $id;
        return $bookauthor;
    }

    public function deleteByBook($bookId){
        $stmt = $this->database->connection->prepare('DELETE FROM Book_Author WHERE Books_ID =:bookId');
        $stmt->bindValue(':bookId', $bookId);
        $stmt->execute();
    }

    public function deleteByAuthor($authorID){
        $stmt = $this->database->connection->prepare('DELETE FROM Book_Author WHERE Authors_ID =:authorID');
        $stmt->bindValue(':authorID', $authorID);
        $stmt->execute();
    }

    public function getAllBooksWithAuthors(){
        $stmt = $this->database->connection->prepare(
            "SELECT Books.ID AS bookID, Books.title AS bookTitle, Books.grade as bookGrade, Books.isRead AS bookRead, Authors.ID AS authID, Authors.fname AS fname, Authors.lname AS lname, Authors.grade AS  authGrade
                        FROM Books
                             LEFT JOIN Book_Author ON Book_Author.Books_ID = Books.ID
                             LEFT JOIN Authors ON Book_Author.Authors_ID = Authors.ID"
        );
        $stmt->execute();
        $result = $stmt->fetchAll();

        $books = [];
        if (count($result) > 0){
            foreach ($result as $row){
                $author = null;
                if (count($books) > 0){
                    $fBook = null;
                    foreach ($books as $book){
                        if ($book->id == $row["bookID"]){
                            $fBook = $book;
                            break;
                        }
                    }
                    if ($fBook !== null){
                        if (isset($row["authID"])){
                            $author = new Author($row["authID"], $row["fname"], $row["lname"], $row["authGrade"]);
                        }
                        $fBook->author2 = $author;
                    }
                    else {
                        if (isset($row["authID"])){
                            $author = new Author($row["authID"], $row["fname"], $row["lname"], $row["authGrade"]);
                        }
                        $book = new Book($row["bookID"], $row["bookTitle"], $author, null, $row["bookGrade"], $row["bookRead"]);
                        $books[] = $book;
                    }
                } else {
                    if (isset($row["authID"])){
                        $author = new Author($row["authID"], $row["fname"], $row["lname"], $row["authGrade"]);
                    }
                    $book = new Book($row["bookID"], $row["bookTitle"], $author, null, $row["bookGrade"], $row["bookRead"]);
           $books[] = $book;
                }
            }
        }

        return $books;
    }

    public function getBookWithAuthors($bookId){
        $stmt = $this->database->connection->prepare(
            "SELECT Books.ID AS bookID, Books.title AS bookTitle, Books.grade as bookGrade, Books.isRead AS bookRead, Authors.ID AS authID, Authors.fname AS fname, Authors.lname AS lname, Authors.grade AS  authGrade
                        FROM Books
                             LEFT JOIN Book_Author ON Book_Author.Books_ID = Books.ID
                             LEFT JOIN Authors ON Book_Author.Authors_ID = Authors.ID
                             WHERE Books.ID =:bookId"
        );
        $stmt->bindValue(':bookId', $bookId);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $book = null;
        if (count($result) > 0){
            foreach ($result as $row){
                $author = null;
                if ($book !== null){
                    if (isset($row["authID"])){
                        $author = new Author($row["authID"], $row["fname"], $row["lname"], $row["authGrade"]);
                    }
                    $book->author2 = $author;
                } else {
                    if (isset($row["authID"])){
                        $author = new Author($row["authID"], $row["fname"], $row["lname"], $row["authGrade"]);
                    }
                    $book = new Book($row["bookID"], $row["bookTitle"], $author, null, $row["bookGrade"], $row["bookRead"]);
                }
            }
        }
        return $book;
    }
}