<?php


class AuthorDAO
{
    public $database;
    public function __construct()
    {
        $this->database = new Database();
    }

    public function add($author)
    {
        $stmt = $this->database->connection->prepare('INSERT INTO Authors (fname, lname, grade) VALUES (:fname, :lname, :grade)');
        $stmt->bindValue(':fname', $author-> fname);
        $stmt->bindValue(':lname', $author-> lname);
        $stmt->bindValue(':grade', $author-> grade);
        $stmt->execute();

        $id = $this->database->connection->lastInsertId();
        $author->id = $id;
        return $author;
    }

    public function get($id){
        $stmt = $this->database->connection->prepare('SELECT * FROM Authors WHERE id = :id');
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $result = $stmt->fetch();
        if (! $result){
            return null;
        }
        return $author = new Author($result['ID'], $result['fname'], $result['lname'], $result['grade']);
    }

    public function update($author){
        $stmt = $this->database->connection->prepare('UPDATE Authors SET fname = :fname, lname =:lname, grade =:grade WHERE id =:id');

        $stmt->bindValue(':id', $author-> id);
        $stmt->bindValue(':fname', $author-> fname);
        $stmt->bindValue(':lname', $author-> lname);
        $stmt->bindValue(':grade', $author-> grade);
        $stmt->execute();

    }

    public function delete($author){
        $stmt = $this->database->connection->prepare('DELETE FROM Authors WHERE id =:id');
        $stmt->bindValue(':id', $author-> id);
        $stmt->execute();
    }

    public function getAuthors(){
        $stmt = $this->database->connection->prepare('SELECT * FROM Authors');
        $stmt->execute();

        $author = [];
        foreach ($stmt as $row){
            $author[] = new Author($row['ID'], $row['fname'], $row['lname'], $row['grade']);
        }
        return $author;
    }

}