<?php

class BookDAO
{
    public $database;
    public function __construct()
    {
        $this->database = new Database();
    }

    public function add($book)
    {
        $stmt = $this->database->connection->prepare('INSERT INTO Books (title, grade, isRead) VALUES (:title, :grade, :isRead)');
        $stmt->bindValue(':title', $book-> title);
        $stmt->bindValue(':grade', $book-> grade);
        $stmt->bindValue(':isRead', $book-> isRead);
        $stmt->execute();

        $id = $this->database->connection->lastInsertId();
        $book->id = $id;
        return $book;
    }

    public function update($book){
        $stmt = $this->database->connection->prepare('UPDATE Books SET title = :title, grade =:grade, isRead =:isRead WHERE id =:id');

        $stmt->bindValue(':id', $book-> id);
        $stmt->bindValue(':title', $book-> title);
        $stmt->bindValue(':grade', $book-> grade);
        $stmt->bindValue(':isRead', $book-> isRead);
        $stmt->execute();

    }

    public function delete($book){
        $stmt = $this->database->connection->prepare('DELETE FROM Books WHERE id =:id');
        $stmt->bindValue(':id', $book-> id);
        $stmt->execute();
    }


}