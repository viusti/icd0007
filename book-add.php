<?php
require_once "./DAO/AuthorDAO.php";
require_once "./DAO/Database.php";
require_once "./DTO/Author.php";
$authors = new AuthorDAO();
$authorData = $authors->getAuthors();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Lisa Raamat</title>

    <link href="styles.css" rel="stylesheet">

</head>
<body id="book-form-page">
    <nav>
        <a id="book-list-link" href="index.php">Raamatud</a>
        <span>|</span>
        <a id="book-form-link" href="book-add.php">Lisa raamat</a>
        <span>|</span>
        <a id="author-list-link" href="author-list.php">Autorid</a>
        <span>|</span>
        <a id="author-form-link" href="author-add.php">Lisa autor</a>
    </nav>
    <?php if (isset($_GET["error"])) : ?>
        <div id="error-block"> Pealkiri peab olema 3 kuni 23 märki! </div>
    <?php endif; ?>

    <main>
    <form action="books.php" method="post">
        <label for="title">Pealkiri:</label>

        <input type="text" id="title" name="title" value="<?php echo isset($_GET["title"]) ? $_GET["title"] : ""?>"><br><br>

        <label for="author1">Autor 1:</label>
        <select name="author1" id="author1">
            <option></option>
            <?php foreach ($authorData as $author): ?>
                <option value="<?= $author->id?>"><?= $author->fname . " " . $author->lname ?></option>>
            <?php endforeach; ?>
        </select>
        <br><br>

        <label for="author2">Autor 2:</label>
        <select name="author2" id="author2">
            <option></option>
            <?php foreach ($authorData as $author): ?>
                <option value="<?= $author->id?>"><?= $author->fname . " " . $author->lname ?></option>>
            <?php endforeach; ?>
        </select>

        <br><br>

        <label>Hinne:</label>

        <?php for ($i = 1; $i <= 5; $i++): ?>
            <input <?php echo (isset($_GET["grade"]) && ($_GET["grade"] == $i)) ? "checked" : ""?> type="radio" name="grade" value="<?= $i ?>"><?= $i ?>
        <?php endfor; ?>

        <br><br>

        <label for="isRead">Loetud:</label>

        <input <?php echo (isset($_GET["isRead"])) ? "checked" : ""?> id="isRead" name="isRead" type="checkbox">

        <br><br>

        <input name="submitButton" type="submit" value="Salvesta">
    </form>
    </main>
    <footer>
        <p>ICD0007 Harjutustunni leht</p>
    </footer>
</body>
</html>