<?php
$firstName = $_POST["firstName"];
$lastName = $_POST["lastName"];
$grade = isset($_POST["grade"]) ? $_POST["grade"] : 0;
$delete = isset($_POST["deleteButton"]);
$id = isset($_GET["id"]) ? $_GET["id"] : null;

if (!is_numeric($grade)){
    $grade = 0;
}

$author = new Author($id, $firstName, $lastName, $grade);
$authorDAO = new AuthorDAO();
$bookAuthorDB = new BoAuDAO();
$error = Author::validate($author);

if ($delete){
    $bookAuthorDB->deleteByAuthor($id);
    $authorDAO->delete($author);
    header("Location: /?cmd=authorlist&message=2"); exit();
}
if ($error) {
    $data['author'] = $author;
    $data['error'] = $error;
    $data['template'] = 'author-edit.html';
    print renderTemplate("templates/base.html", $data);
}
else if ($id) {
    $authorDAO->update($author);
    header("Location: /?cmd=authorlist&message=1"); exit();
} else {
    $authorDAO->add($author);

    header("Location: /?cmd=authorlist&message=3"); exit();
}


