<?php


class Author
{
    public $id;
    public $fname;
    public $lname;
    public $grade;

    public function __construct($id, $fname, $lname, $grade)
    {
        $this->id = $id;
        $this->fname = $fname;
        $this->lname = $lname;
        $this->grade = $grade;
    }

    public static function validate($author){
        if (strlen($author->fname) > 21 or strlen($author->fname) < 1 or strlen($author->lname) > 22 or strlen($author->lname) < 2){
            return "Eesnimi peab olema 1 - 21 tähemärki ja Perekonnanimi peab olema 2 - 22 tähemärki.";
        }
        return null;
    }

}