<?php


class Book
{
    public $id;
    public $title;
    public $author1;
    public $author2;
    public $grade;
    public $isRead;

    public function __construct($id, $title, $author1, $author2, $grade, $isRead)
    {
        $this->id = $id;
        $this->title = $title;
        $this->author1 = $author1;
        $this->author2 = $author2;
        $this->grade = $grade;
        $this->isRead = $isRead;
    }

    public static function validate($book){
        if (strlen($book->title) < 3 or strlen($book->title) > 23){
            return "Pealkiri peab olema 3 kuni 23 märki!";
        }
        if ($book->author1 !== null && $book->author2 !== null){
            if ($book->author1->id === $book->author2->id){
                return "Autor 1 ja autor 2 ei tohi olla samad.";
            }
        }
        return null;
    }

}