<?php


class BookAuthor
{
    public $id;
    public $bookId;
    public $authorId;

    public function __construct($id, $bookId, $authorId)
    {
        $this->id = $id;
        $this->bookId = $bookId;
        $this->authorId = $authorId;
    }

}