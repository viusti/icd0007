<?php
require_once "./DAO/AuthorDAO.php";
require_once "./DAO/Database.php";
require_once "./DTO/Author.php";
$authors = new AuthorDAO();
$authorData = $authors->getAuthors();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Autorid</title>

    <link href="styles.css" rel="stylesheet">

</head>
<body id="author-list-page">
<nav>
    <a id="book-list-link" href="index.php">Raamatud</a>
    <span>|</span>
    <a id="book-form-link" href="book-add.php">Lisa raamat</a>
    <span>|</span>
    <a id="author-list-link" href="author-list.php">Autorid</a>
    <span>|</span>
    <a id="author-form-link" href="author-add.php">Lisa autor</a>
</nav>
<br>
<?php if (isset($_GET["added"])) : ?>
    <div id="message-block"> Lisatud. </div>
<?php endif; ?>
<?php if (isset($_GET["updated"])) : ?>
    <div id="message-block"> Uuendatud. </div>
<?php endif; ?>
<?php if (isset($_GET["deleted"])) : ?>
    <div id="message-block"> Kustutatud. </div>
<?php endif; ?>
<main>
    <table>
        <tr>
            <th>Nimi</th>
            <th>Perekonnanimi</th>
            <th>Hinne</th>
        </tr>
        <?php foreach ($authorData as $author): ?>
            <tr>
                <td><a href="author-edit.php?old_fname=<?= urlencode($author->fname) ?>&old_lname=<?= urlencode($author->lname) ?>&old_grade=<?= $author->grade ?>&id=<?= $author->id?>"><?= $author->fname ?></a></td>
                <td><?= $author->lname ?></td>
                <td><?= $author->grade ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</main>
<footer>
    <p>ICD0007 Harjutustunni leht</p>
</footer>
</body>
</html>