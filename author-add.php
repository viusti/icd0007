<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Lisa autor</title>

    <link href="styles.css" rel="stylesheet">

</head>
<body id="author-form-page">
    <nav>
        <a id="book-list-link" href="index.php">Raamatud</a>
        <span>|</span>
        <a id="book-form-link" href="book-add.php">Lisa raamat</a>
        <span>|</span>
        <a id="author-list-link" href="author-list.php">Autorid</a>
        <span>|</span>
        <a id="author-form-link" href="author-add.php">Lisa autor</a>
    </nav>
    <?php if (isset($_GET["error"])) : ?>
    <div id="error-block"> Eesnimi peab olema 1 - 21 tähemärki ja Perekonnanimi peab olema 2 - 22 tähemärki.</div>
    <?php endif; ?>
    <form action="authors.php" method="post">
        <label for="fname">Eesnimi:</label>
        <br>
        <input  type="text" id="fname" name="firstName" value="<?php echo isset($_GET["firstName"]) ? $_GET["firstName"] : ""?>">
        <br><br>
        <label for="lname">Perekonnanimi:</label>
        <br>
        <input type="text" id="lname" name="lastName" value="<?php echo isset($_GET["lastName"]) ? $_GET["lastName"] : ""?>">
        <br><br>
        <label>Hinne:</label>
        <?php for ($i = 1; $i <= 5; $i++): ?>
            <input <?php echo (isset($_GET["grade"]) && ($_GET["grade"] == $i)) ? "checked" : ""?> type="radio" name="grade" value="<?= $i ?>"><?= $i ?>
        <?php endfor; ?>
        <input name="submitButton" type="submit" value="Salvesta">
    </form>
    <footer>
        <p>ICD0007 Harjutustunni leht</p>
    </footer>
</body>
</html>