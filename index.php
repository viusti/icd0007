<?php
require 'tpl.php';
require_once "./DAO/BookDAO.php";
require_once "./DAO/Database.php";
require_once "./DTO/Book.php";
require_once "./DAO/BoAuDAO.php";
require_once "./DTO/BookAuthor.php";
require_once "./DAO/AuthorDAO.php";
require_once "./DTO/Author.php";
$authorDAO = new AuthorDAO();
$bookAuthorDAO = new BoAuDAO();
$data = [];

$cmd = "booklist";
if (isset($_GET["cmd"])) {
    $cmd = $_GET["cmd"];
}if ($cmd === "booklist"){
    $bookAuthorData = $bookAuthorDAO->getAllBooksWithAuthors();
    $data['bookAuthorData'] = $bookAuthorData;
    $data['pageId'] = "book-list-page";
    $contentPath = 'book-list.html';
}else if ($cmd === "bookedit"){
    $data['authorData'] = $authorDAO->getAuthors();
    $data['book'] =(isset($_GET['id'])) ? $bookAuthorDAO->getBookWithAuthors($_GET['id']): null;
    $contentPath = 'book-edit.html';
    $data['pageId'] = "book-form-page";
}else if ($cmd === "bookpost"){
    include 'books.php';
    exit();
}else if ($cmd === "authorlist"){
    $data['authorData'] = $authorDAO->getAuthors();
    $contentPath = 'author-list.html';
    $data['pageId'] = "author-list-page";
}else if ($cmd === "authorpost"){
    include 'authors.php';
    exit();
}else if ($cmd === "authoredit"){
    $data['author'] = (isset($_GET['id'])) ? $authorDAO->get($_GET['id']): null;
    $contentPath = 'author-edit.html';
    $data['pageId'] = "author-form-page";
}else {
    echo "404";
}
$data['template'] = $contentPath;
print renderTemplate("templates/base.html", $data);