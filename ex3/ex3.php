<?php

$numbers = [1, 2, 5, 6, 2, 11, 2, 7];
print_r(getOddNumbers($numbers));

function getOddNumbers($numbers){
    $oddNumbers = [];
    foreach ($numbers as $number){
        if ($number % 2 === 1){
            $oddNumbers[] = $number;
        }
    }
    return $oddNumbers;
}
