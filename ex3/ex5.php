<?php

//Kirjutage programm, mis võtab sisendiks listi ja tagastab stringi,
//   milles on kõik elemendid komaga eraldatuna ning seejärel teeb
//   stringist esialgse listi tagasi. Kasutage olemasolevaid funktsioone
//   join() ja explode().
//
//[1, 2, 3] -> "1, 2, 3" -> [1, 2, 3]

$numbers = [3, 2, 5, 6];

$joined = join(", ", $numbers);
print $joined;

$exploded = explode(", ", $joined);
print_r($exploded);
