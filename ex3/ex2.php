<?php

$numbers = [1, 2, '3', 6, 2, 3, 2, 3];


function isInList($numbers, $elementToBeFound){
    $found = false;
    foreach ($numbers as $number){
        if ($number === $elementToBeFound){
            $found = true;
            break;
        }
    }
    return $found;
}

