<?php
$id = isset($_GET["id"]) ? $_GET["id"] : null;
$title = $_POST["title"];
$author1 = ($_POST["author1"] != "" && is_numeric($_POST["author1"])) ? $_POST["author1"] : null;
$author2 = ($_POST["author2"] != "" && is_numeric($_POST["author2"])) ? $_POST["author2"] : null;
$isRead = isset($_POST["isRead"]) ? 1 : 0;
$grade = (isset($_POST["grade"]) && is_numeric($_POST["grade"])) ? $_POST["grade"] : 0;
$delete = isset($_POST["deleteButton"]);
$book = new Book($id, $title, null, null, $grade, $isRead);
$bookDAO = new BookDAO();
$bookAuthorDAO = new BoAuDAO();
if ($author1){
    $book->author1 = new Author($author1, null, null, null);
}
if ($author2){
    $book->author2 = new Author($author2, null, null, null);
}
$error = Book::validate($book);

if ($delete){
    $bookAuthorDAO->deleteByBook($book->id);
    $bookDAO->delete($book);
    header("Location: /?cmd=booklist&message=2"); exit();
}
if ($error){
    $authorDAO = new AuthorDAO();
    $data['authorData'] = $authorDAO->getAuthors();
    $data['book'] = $book;
    $data['error'] = $error;
    $data['template'] = 'book-edit.html';
    print renderTemplate("templates/base.html", $data);
} else if ($id) {
    $bookDAO->update($book);
    $bookAuthorDAO->deleteByBook($book->id);
    if ($book->author1 !== null) {
        $bookAuthorAdd = new BookAuthor(null, $book->id, $book->author1->id);
        $bookAuthorDAO->add($bookAuthorAdd);
    }
    if ($book->author2 !== null) {
        $bookAuthorAdd = new BookAuthor(null, $book->id, $book->author2->id);
        $bookAuthorDAO->add($bookAuthorAdd);
    }
    header("Location: /?cmd=booklist&message=1");
} else {
    $bookDAO->add($book);
    if ($book->author1 !== null){
        $bookAuthor = new BookAuthor(null, $book->id, $book->author1->id);
        $bookAuthorDAO->add($bookAuthor);
    }
    if ($book->author2 !== null){
        $bookAuthor = new BookAuthor(null, $book->id, $author2);
        $bookAuthorDAO->add($bookAuthor);
    }
    header("Location: /?cmd=booklist&message=3");
}






