<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Lisa autor</title>

    <link href="styles.css" rel="stylesheet">

</head>
<body>
<nav>
    <a id="book-list-link" href="index.php">Raamatud</a>
    <span>|</span>
    <a id="book-form-link" href="book-add.php">Lisa raamat</a>
    <span>|</span>
    <a id="author-list-link" href="author-list.php">Autorid</a>
    <span>|</span>
    <a id="author-form-link" href="author-add.php">Lisa autor</a>
</nav>
<?php if (isset($_GET["error"])) : ?>
    <div id="error-block"> Eesnimi peab olema 1 - 21 tähemärki ja Perekonnanimi peab olema 2 - 22 tähemärki.</div>
<?php endif; ?>
<form action="authors.php" method="post">
    <label for="fname">Eesnimi:</label>
    <br>
    <input type="text" id="fname" name="firstName" value="<?= $_GET["old_fname"]?>">
    <input type="hidden" id="old_fname" name="old_fname" value="<?= $_GET["old_fname"]?>">
    <br><br>
    <label for="lname">Perekonnanimi:</label>
    <br>
    <input type="text" value="<?= $_GET["old_lname"]?>" id="lname" name="lastName">
    <input type="hidden" id="old_lname" name="old_lname" value="<?= $_GET["old_lname"]?>">
    <input type="hidden" id="id" name="id" value="<?= isset($_GET["id"]) ? $_GET["id"] : ""?>">
    <br><br>
    <label>Hinne:</label>
    <?php for ($i = 1; $i <= 5; $i++): ?>
        <input <?php echo ($_GET["old_grade"] == $i) ? "checked" : ""?> type="radio" name="grade" value="<?= $i ?>"><?= $i ?>
    <?php endfor; ?>
    <input name="deleteButton" type="submit" value="Kustuta">
    <input name="submitButton" type="submit" value="Salvesta">
</form>
<footer>
    <p>ICD0007 Harjutustunni leht</p>
</footer>
</body>
</html>